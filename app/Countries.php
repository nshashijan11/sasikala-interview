<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    protected $table = 'countries';

    public function city(){
   		return $this->hasMany('\App\City');
   	}

   	public function state(){
   		return $this->hasMany('\App\States');
   	}

   	public function others()
    {
        return $this->hasMany('App\Others')->orderBy('country_name'.'asc');
    }

}
