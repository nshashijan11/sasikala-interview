<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
   	protected $table = 'city';

   	public function countries(){
   		return $this->belongsTo('\App\Countries','country_id', 'id');
   	}

   	public function states(){
   		return $this->belongsTo('\App\States', 'state_id', 'id');
   	}

   	public function others()
    {
        return $this->hasMany('App\Others');
    }

   	
   	
}
