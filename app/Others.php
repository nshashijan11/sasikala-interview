<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Others extends Model
{
    protected $table = 'others';

    public function countries(){
   		return $this->belongsTo('\App\Countries','country_id', 'id');
   	}

   	public function states(){
   		return $this->belongsTo('\App\States', 'state_id', 'id');
   	}

    public function cities(){
    	return $this->belongsTo('\App\City', 'city_id', 'id');
    }


    // public function scopeOtherLevels($query) {
    //     return $query->join('city', function ($join) {
    //                 $join->on('city.id', '=', 'others.city_id');
    //             })
    //             ->join('states', function ($join) {
    //                 $join->on('states.id', '=', 'others.state_id');
    //             })
    //              ->join('countries', function ($join) {
    //                 $join->on('countries.id', '=', 'others.country_id');
                         
    //             })
    //             ->orderBy('countries.country_name','asc')
    //             ->get();
    // }


}
