<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Countries;
use \App\States;
use \App\City;
use \App\Others;
use Exception;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * CRUD for  Cities,.
     *
     * @return \Illuminate\Http\Response
     */
    public function ccsList()
    {

        $coun = $stat = $cit = $are = '';
        $ccsList = DB::table('others')
                   ->join('city', 'city.id', '=', 'others.city_id')
                   ->join('states', 'states.id', '=', 'others.state_id')
                   ->join('countries', 'countries.id', '=', 'others.country_id')
                   ->select('countries.country_name', 'states.state_name', 'city.city_name','others.area_name','others.address_id','others.id' )
                   ->orderBy('countries.country_name','asc')
                   ->get(); 
        // $ccsList = Others::with('countries','states','cities')->get();
        // $ccsList = Others::with('otherlevels')->get();

        return view('list', ['ccs_list' => $ccsList, 'coun' => $coun, 'stat' => $stat,'cit' => $cit, 'are' => $are]);
    }

    /**
     * CRUD for Countries,.
     *
     * @return \Illuminate\Http\Response
     */
    public function countries()
    {
        $countries = Countries::all();

        return view('countries', ['countries' => $countries]);
    }

    public function submitCountry(Request $request)
    {
        $response = [];
        try{
            if($request->all()){
                
                $check = self::checkCountry($request->all());
                if($request->id){
                    $country = Countries::find($request->id);
                    $msg = " Updated!";   
                }else{                   
                    $country = new Countries();  
                    $msg =" Added!";                  
                }
                $country->country_name = $request->country_name;
                if($country->save()){
                    $response['msg'] = 'Country Name'.$msg;
                    $response['err'] = 'true';
                    echo json_encode($response);
                    exit;
                }                
                $response['msg'] = 'Error Occured!';
                $response['err'] = 'false';
                echo json_encode($response);
                exit;
            }
        }catch(\Exception $e){
            throw New Exception($e->getMessage());
        }
    }

    public function checkCountry($request){

        if(isset($request['id'])){
           
            $country = Countries::where('country_name', $request['country_name'])->where('id', '!=', $request['id'])->get();
        }else{
             $country = Countries::where('country_name', $request['country_name'])->get();
        }
        
        if(count($country)){
            $response['msg'] = 'Country Name already exist!';
            $response['err'] = 'false';
            echo json_encode($response);
            exit;
        }
    }

   

    public function getCountryDetails(Request $request)
    {
         try{
            if($request->all()){
                $cities = Countries::find($request->country_id);
                if($cities){
                    return $cities;
                }
            }
        }catch(\Exception $e){
            throw New Exception($e->getMessage());
        }
    }


     public function deleteCountryDetails(Request $request)
    {
         try{
            if($request->all()){
                $cities = Countries::find($request->country_id);
                if($cities){
                    $cities->delete();
                    return 'true';
                }
            }
        }catch(\Exception $e){
            throw New Exception($e->getMessage());
        }
    }


    


    /**
     * CRUD for  States,.
     *
     * @return \Illuminate\Http\Response
     */
    public function states()
    {
        $countries = Countries::select('id','country_name')->get();
        $states = States::with('countries')->get();

        return view('states', ['countries' => $countries,'states' => $states ]);
    }

    public function submitState(Request $request)
    {
        $response = [];
        try{
            if($request->all()){
                $check = self::checkState($request->all());
                if($request->id){
                     $states = States::find($request->id);
                     $msg =" Updated!";
                }else{
                    $states = new States(); 
                    $msg =" Added!";                   
                }
                $states->country_id = $request->countries;
                $states->state_name = $request->state_name;
               
                if($states->save()){
                    $response['msg'] = 'State Details'.$msg;
                    $response['err'] = 'true';
                    echo json_encode($response);
                    exit;
                }                
                $response['msg'] = 'Error Occured!';
                $response['err'] = 'false';
                echo json_encode($response);
                exit;

                
            }
        }catch(\Exception $e){
            throw New Exception($e->getMessage());
        }
    }

     public function checkState($request){
        if(isset($request['id'])){
           
            $state = States::where('state_name', $request['state_name'])->where('country_id',$request['countries'])->where('id', '!=', $request['id'])->get();
        }else{
             $state = States::where('state_name', $request['state_name'])->where('country_id',$request['countries'])->get();
        }
        
        if(count($state)){
            $response['msg'] = 'State Name already exist!';
            $response['err'] = 'false';
            echo json_encode($response);
            exit;
        }
    }

    public function getStateDetails(Request $request)
    {
         try{
            if($request->all()){
                $states = States::find($request->state_id);
                if($states){
                    return $states;
                }
            }
        }catch(\Exception $e){
            throw New Exception($e->getMessage());
        }
    }


    public function deleteStateDetails(Request $request)
    {
         try{
            if($request->all()){
                $states = States::find($request->state_id);
                if($states){
                    $states->delete();
                    return 'true';
                }
            }
        }catch(\Exception $e){
            throw New Exception($e->getMessage());
        }
    }

    /**
     * CRUD for  Cities,.
     *
     * @return \Illuminate\Http\Response
     */
    public function city()
    {
        $countries = Countries::select('id','country_name')->get();
        $states = States::select('id','state_name')->get();
        $cities = City::with('countries')->with('states')->get();

        return view('city', ['countries' => $countries,'states' => $states ,'cities' => $cities]);
    }


    public function submitCity(Request $request)
    {
        $response = [];
        try{
            if($request->all()){
                $check = self::checkCity($request->all());
                if($request->id){
                     $city = City::find($request->id);
                     $msg =" Updated!";
                }else{
                    $city = new City();     
                    $msg =" Added!";               
                }
                $city->country_id = $request->countries;
                $city->state_id = $request->states;
                $city->city_name = $request->city_name;
                if($city->save()){
                    $response['msg'] = 'City Details'.$msg;
                    $response['err'] = 'true';
                    echo json_encode($response);
                    exit;
                }                
                $response['msg'] = 'Error Occured!';
                $response['err'] = 'false';
                echo json_encode($response);
                exit;

                
                return 'false';
            }
        }catch(\Exception $e){
            throw New Exception($e->getMessage());
        }
    }

    public function checkCity($request){
        if(isset($request['id'])){
           
            $state = City::where('city_name', $request['city_name'])->where('country_id',$request['countries'])->where('state_id',$request['states'])->where('id', '!=', $request['id'])->get();
        }else{
             $state = City::where('city_name', $request['city_name'])->where('country_id',$request['countries'])->where('state_id',$request['states'])->get();
        }
        
        if(count($state)){
            $response['msg'] = 'City Name already exist!';
            $response['err'] = 'false';
            echo json_encode($response);
            exit;
        }
    }

    public function getCityDetails(Request $request)
    {
         try{
            if($request->all()){
                $cities = City::find($request->city_id);
                if($cities){
                    return $cities;
                }
            }
        }catch(\Exception $e){
            throw New Exception($e->getMessage());
        }
    }


     public function deleteCityDetails(Request $request)
    {
         try{
            if($request->all()){
                $cities = City::find($request->city_id);
                if($cities){
                    $cities->delete();
                    return 'true';
                }
            }
        }catch(\Exception $e){
            throw New Exception($e->getMessage());
        }
    }
        


    public function getStateFromCountry(Request $request)
    {
        try{
            if($request->all()){
                $states = States::where('country_id', $request->country_id)->orderBy('state_name')->get();
                if($states){
                    return $states;
                }
            }
        }catch(\Exception $e){
            throw New Exception($e->getMessage());
        }
    }


    public function getCityFromState(Request $request)
    {
        try{
            if($request->all()){
                $cities = City::where('country_id', $request->country_id)->where('state_id', $request->state_id)->orderBy('city_name')->get();
                if($cities){
                    return $cities;
                }
            }
        }catch(\Exception $e){
            throw New Exception($e->getMessage());
        }
    }


    /**
     * CRUD for  other Levels,.
     *
     * @return \Illuminate\Http\Response
     */
    public function others()
    {
        $countries = Countries::select('id','country_name')->get();
        $states = States::select('id','state_name')->get();
        $cities = City::select('id','city_name')->get();
        $others = Others::with('countries','states')->with('cities')->get();
        return view('otherlevels', ['countries' => $countries,'states' => $states ,'cities' => $cities,'others' => $others]);
    }


    public function submitOthers(Request $request)
    {

        // print_r($request->all()); 

        $response = [];
        try{
            if($request->all()){
                $check = self::checkAreaName($request->all());
                if($request->id){
                     $otherlevels = Others::find($request->id);
                     $msg =" Updated!";
                }else{
                    $otherlevels = new Others();     
                    $msg =" Added!";               
                }
                $otherlevels->country_id = $request->countries;
                $otherlevels->state_id = $request->states;
                $otherlevels->city_id = $request->cities;
                $otherlevels->area_name = $request->area_name;
                if(isset($request->area_name)){
                    $otherlevels->label = 'area';
                    $otherlevels->value = $request->area_name;
                }else{
                    $otherlevels->label = '';
                    $otherlevels->value = $request->area_name;
                }
                $labelName = $request->label_name;
                $labelValue = $request->label_value;
                $arr = [];
                if(count($labelName) == count($labelValue)){

                    foreach ($labelName as $keyL => $valueL) {
                        $lblDyn = $valueL;
                        foreach ($labelValue as $keyV => $valueV) {
                            if($keyL == $keyV){
                                $valueDyn = $valueV;
                                $arr[$lblDyn] = $valueDyn;
                            }                            
                        }
                    }
                }
                $otherlevels->address_id = json_encode($arr);
                
                if($otherlevels->save()){
                    $response['msg'] = 'Other Details'.$msg;
                    $response['err'] = 'true';
                    echo json_encode($response);
                    exit;
                }                
                $response['msg'] = 'Error Occured!';
                $response['err'] = 'false';
                echo json_encode($response);
                exit;               
                return 'false';
            }
        }catch(\Exception $e){
            throw New Exception($e->getMessage());
        }
    }
    

     public function checkAreaName($request){
        if(isset($request['id'])){
           
            $otherlevels = Others::where('area_name', $request['area_name'])->where('country_id',$request['countries'])->where('state_id',$request['states'])->where('city_id',$request['cities'])->where('id', '!=', $request['id'])->get();
        }else{
             $otherlevels = Others::where('area_name', $request['area_name'])->where('country_id',$request['countries'])->where('state_id',$request['states'])->where('city_id',$request['cities'])->get();
        }
        
        if(count($otherlevels)){
            $response['msg'] = 'Area Name already exist!';
            $response['err'] = 'false';
            echo json_encode($response);
            exit;
        }
    }


    public function getOtherDetails(Request $request)
    {
         try{
            if($request->all()){
                $others = Others::find($request->id);
                if($others){
                    return $others;
                }
            }
        }catch(\Exception $e){
            throw New Exception($e->getMessage());
        }
    }


     public function deleteOtherDetails(Request $request)
    {
         try{
            if($request->all()){
                $cities = Others::find($request->id);
                if($cities){
                    $cities->delete();
                    return 'true';
                }
            }
        }catch(\Exception $e){
            throw New Exception($e->getMessage());
        }
    }

}
