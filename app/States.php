<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    protected $table = 'states';


    public function countries(){
    	return $this->belongsTo('\App\Countries', 'country_id', 'id');
    }

    public function others()
    {
        return $this->hasMany('App\Others');
    }
}	
