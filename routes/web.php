<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('countries', 'HomeController@countries');
Route::post('submitCountry', 'HomeController@submitCountry');
Route::get('getCountryDetails', 'HomeController@getCountryDetails');
Route::get('deleteCountryDetails', 'HomeController@deleteCountryDetails');

Route::get('states', 'HomeController@states');
Route::post('submitState', 'HomeController@submitState');
Route::get('getStateDetails', 'HomeController@getStateDetails');
Route::get('deleteStateDetails', 'HomeController@deleteStateDetails');

Route::post('getStateFromCountry', 'HomeController@getStateFromCountry');
Route::post('getCityFromState', 'HomeController@getCityFromState');



Route::get('city', 'HomeController@city');
Route::post('submitCity', 'HomeController@submitCity');
Route::get('getCityDetails', 'HomeController@getCityDetails');
Route::get('deleteCityDetails', 'HomeController@deleteCityDetails');

Route::get('list', 'HomeController@ccsList');

Route::get('others', 'HomeController@others');
Route::post('submitOthers', 'HomeController@submitOthers');
Route::get('getOtherDetails', 'HomeController@getOtherDetails');
Route::get('deleteOtherDetails', 'HomeController@deleteOtherDetails');