$(document).ready(function(){


	$.ajaxSetup({
		headers:{
			'X-CSRF-TOKEN' :$('meta[name="csrf-token"]').attr('content')
		}
	});



	$('#myForm').on('submit', function(e) {

       e.preventDefault(); 

       var country_id = $('#id').val();    
       var country_name = $('#country_name').val();    
       if($.trim(country_name) == ''){
       		alert('Please Enter the Country Name');
       		$('#country_name').val('');
       		$('#country_name').focus();
       }

       $.ajax({
           type: "POST",
           url: 'submitCountry',
           data: $('#myForm').serialize(),
           success: function( data ) {
           		var obj = jQuery.parseJSON(data);
	           	if (obj.err === 'true'){
	           		alert(obj.msg);
	           		window.location.reload();
	           	}else if (obj.err=== 'false'){
	           		alert(obj.msg);
	           		$('country_name').focus();
	           	}
	       }
  		});

	});

	 $('.editCountry').on('click', function(e) {
	 	var country_id = $(this).attr('data-id');
	    $.ajax({
			type: "GET",
			url: 'getCountryDetails',
			data: {'country_id':country_id},
			success: function( data ) {
			   	if (data){
			       	$.each(data,function(key,value){
			       		 if(key == 'country_name'){
			       			$("#country_name").val(value);
			       		}else if(key == 'id'){
			       			$("#id").val(value);
			       		}
			        });
			   	}else{
			   		alert('Error Occurrecd!');
			   	}
			}
		});

	 });

	 $('.deleteCountry').on('click', function(e) {
	 	var country_id = $(this).attr('data-id');
	    if (confirm("Are you sure to delete?")) { 
	       	$.ajax({
				type: "GET",
				url: 'deleteCountryDetails',
				data: {'country_id':country_id},
				success: function( msg ) {
				   	if (msg == 'true'){
				       	alert('Country Deleted!');
				       	 window.location.reload();
				   	}else{
				   		alert('Error Occurrecd!');
				   	}
				}
			});
	    } 

	 });

	 
	// $('#countries').change(function(){
 //    	var country_id = $(this).val();    
	//     if(country_id){
	//         $.ajax({
	//            type:"POST",
	//            url:"getStateFromCountry",
	//            data: {'country_id' : country_id},
	//            success:function(res){               
	//             if(res){
	//                 $("#states").empty();
	//                 $("#states").append('<option>Select State</option>');
	//                 $.each(res,function(key,value){
	//                     $("#states").append('<option value="'+value.id+'">'+value.state_name+'</option>');
	//                 });
	           
	//             }else{
	//                $("#states").empty();
	//             }
	//            }
	//         });
	//     }else{
	//         $("#states").empty();
	//         $("#cities").empty();
	//     }      
	//   });


	// $('#states').on('change',function(){
	//     var country_id = $('#countries').val();    
	//     var state_id = $(this).val();    
	//     if(state_id){
	//         $.ajax({
	//            type: "POST",
	//            url:"getCityFromState",
	//            data: {'country_id' : country_id , 'state_id' : state_id},	           
	//            success:function(res){               
	//             if(res){
	//                 $("#cities").empty();
	//                 $("#cities").append('<option>Select City</option>');
	//                 $.each(res,function(key,value){
	//                     $("#cities").append('<option value="'+value.id+'">'+value.city_name+'</option>');
	//                 });
	           
	//             }else{
	//                $("#cities").empty();
	//             }
	//            }
	//         });
	//     }else{
	//         $("#cities").empty();
	//     }


	// });



	 
});