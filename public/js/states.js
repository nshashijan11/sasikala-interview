$(document).ready(function(){


	$.ajaxSetup({
		headers:{
			'X-CSRF-TOKEN' :$('meta[name="csrf-token"]').attr('content')
		}
	});

	
	 $('#myForm').on('submit', function(e) {

       e.preventDefault(); 
       var state_id = $('#id').val();    
       var state_name = $('#state_name').val();    
       if($.trim(state_name) == ''){
       		alert('Please Enter the State Name');
       		$('#state_name').val('');
       		$('#state_name').focus();
       }   

       $.ajax({
           type: "POST",
           url: 'submitState',
           data: $('#myForm').serialize(),
           success: function( data ) {
	           	var obj = jQuery.parseJSON(data);
	           	if (obj.err === 'true'){
	           		alert(obj.msg);
	           		window.location.reload();
	           	}else if (obj.err=== 'false'){
	           		alert(obj.msg);
	           		$('country_name').focus();
	           	}
	       }
  		});

	});

	 $('.editState').on('click', function(e) {
	 	var state_id = $(this).attr('data-id');
	    $.ajax({
			type: "GET",
			url: 'getStateDetails',
			data: {'state_id':state_id},
			success: function( data ) {
			   	if (data){
			       	$.each(data,function(key,value){
			       		if(key == 'country_id'){
			       			$("#countries option[value='"+value+"']").attr("selected", "selected");
			       		}else if(key == 'state_name'){
			       			$("#state_name").val(value);
			       		}else if(key == 'id'){
			       			$("#id").val(value);
			       		}
			        });
			   	}else{
			   		alert('Error Occurrecd!');
			   	}
			}
		});

	 });

	 $('.deleteState').on('click', function(e) {
	 	var state_id = $(this).attr('data-id');
	    if (confirm("Are you sure to delete?")) { 
	       	$.ajax({
				type: "GET",
				url: 'deleteStateDetails',
				data: {'state_id':state_id},
				success: function( msg ) {
				   	if (msg == 'true'){
				       	alert('State Deleted!');
				       	 window.location.reload();
				   	}else{
				   		alert('Error Occurrecd!');
				   	}
				}
			});
	    } 

	 });
	

});

