$(document).ready(function(){


	$.ajaxSetup({
		headers:{
			'X-CSRF-TOKEN' :$('meta[name="csrf-token"]').attr('content')
		}
	});

	$('#countries').change(function(){
    	var country_id = $(this).val(); 
	    if(country_id){
	        $.ajax({
	           type:"POST",
	           url:"getStateFromCountry",
	           data: {'country_id' : country_id},
	           success:function(res){               
	            if(res){
	                $("#states").empty();
	                $("#states").append('<option>Select State</option>');
	                $.each(res,function(key,value){
	                    $("#states").append('<option value="'+value.id+'">'+value.state_name+'</option>');
	                });
	           
	            }else{
	               $("#states").empty();
	            }
	           }
	        });
	    }else{
	        $("#states").empty();
	        $("#cities").empty();
	    }      
	  });

	$('#states').change(function(){
    	var country_id = $('#countries').val(); 
    	var state_id = $(this).val(); 
	    if(state_id){
	        $.ajax({
	           type:"POST",
	           url:"getCityFromState",
	           data: {'country_id' : country_id ,'state_id' : state_id},
	           success:function(res){               
	            if(res){
	                $("#cities").empty();
	                $("#cities").append('<option>Select City</option>');
	                $.each(res,function(key,value){
	                    $("#cities").append('<option value="'+value.id+'">'+value.city_name+'</option>');
	                });
	           
	            }else{
	               $("#cities").empty();
	            }
	           }
	        });
	    }else{
	        $("#states").empty();
	        $("#cities").empty();
	    }      
	  });

	 $('#myForm').on('submit', function(e) {

       e.preventDefault(); 
       var city_id = $('#id').val();    
       var area_name = $('#area_name').val();    
       if($.trim(area_name) == ''){
       		alert('Please Enter the Area Name');
       		$('#area_name').val('');
       		$('#area_name').focus();
       }   
       
       $.ajax({
           type: "POST",
           url: 'submitOthers',
           data: $('#myForm').serialize(),
           success: function( data ) {
	           	var obj = jQuery.parseJSON(data);
	           	if (obj.err === 'true'){
	           		alert(obj.msg);
	           		window.location.reload();
	           	}else if (obj.err=== 'false'){
	           		alert(obj.msg);
	           		$('country_name').focus();
	           	}
	       }
  		});

	});

	 $('.editOthers').on('click', function(e) {
	 	var id = $(this).attr('data-id');
	    $.ajax({
			type: "GET",
			url: 'getOtherDetails',
			data: { 'id' : id },
			success: function( data ) {
			   	if (data){
			       	$.each(data,function(key,value){
			       		if(key == 'country_id'){
			       			$("#countries option[value='"+value+"']").attr("selected", "selected");
			       		}else if(key == 'state_id'){
			       			$("#states option[value='"+value+"']").attr("selected", "selected");
			       		}else if(key == 'city_id'){
			       			$("#cities option[value='"+value+"']").attr("selected", "selected");
			       		}else if(key == 'area_name'){
			       			$("#area_name").val(value);
			       		}else if(key == 'id'){
			       			$("#id").val(value);
			       		}else if(key == 'address_id'){
			       			$.each(JSON.parse(value),function(keyV,valV){
			       				var fields = '<div class="form-group dynamic_child">'+
			       								'<label for="'+keyV+'" class="col-md-4 control-label">'+keyV+'</label>' +
												'<div class="col-md-6">'+
												'<input  id="'+keyV+'" type="text" class="form-control" name="label_value[]" value="'+valV+'" required>'+
												'<input  id="'+keyV+'" type="hidden" class="form-control" name="label_name[]" value="'+keyV+'">'+
												'</div><span>'+
												'<i class="fa fa-minus-square remove_dynamic"  title="Remove"></i>'+
												'</span></div>';
								$('.dynamic_fields').append(fields);
			                });
			       		}
			        });
			   	}else{
			   		alert('Error Occurrecd!');
			   	}
			}
		});

	 });

	 $('.deleteOthers').on('click', function(e) {
	 	var id = $(this).attr('data-id');
	    if (confirm("Are you sure to delete?")) { 
	       	$.ajax({
				type: "GET",
				url: 'deleteOtherDetails',
				data: { 'id' : id },
				success: function( msg ) {
				   	if (msg == 'true'){
				       	alert('Others Deleted!');
				       	 window.location.reload();
				   	}else{
				   		alert('Error Occurrecd!');
				   	}
				}
			});
	    }
	 });
	


	$('.add_dynamic').on('click', function(){
		$('#label_name').val('');
		$('#label_value').val('');
		var area_name = $.trim($('#area_name').val());
		if(area_name == ''){
			$('#DynamicForm').modal('hide');
			alert('Please Enter the Area Name');
			$('#area_name').val();  
			$('#area_name').focus();
			return false;
		}
		
	});


	$('.dynamic_fields').on('click', 'i.remove_dynamic', function(events){
		if (confirm("Are you sure to remove the field?")) { 
	   		$(this).parents('div').eq(0).remove();
		}
	});


	$('#modalDynamicForm').on('submit', function(e) {

       e.preventDefault(); 
       var label_name = $.trim($('#label_name').val());    
       var label_value = $.trim($('#label_value').val());  
       if($.trim(label_name) == ''){
       		alert('Please Enter the Label Name');
       		$('#label_name').val('');
       		$('#label_name').focus();
       		return false;
       } else if($.trim(label_value) == ''){
       		alert('Please Enter the Label Value');
       		$('#label_value').val('');
       		$('#label_value').focus();
       		return false;
       }
       var fields = '<div class="form-group dynamic_child"><label for="'+label_name+'" class="col-md-4 control-label">'+label_name+'</label>' +
		'<div class="col-md-6"><input  id="'+label_name+'" type="text" class="form-control" name="label_value[]" value="'+label_value+'" required>'+
		'<input  id="'+label_name+'" type="hidden" class="form-control" name="label_name[]" value="'+label_name+'">'+
		'</div><span><i class="fa fa-minus-square remove_dynamic"  title="Add N-level of Locations"></i></span></div>';
		$('.dynamic_fields').append(fields);
		$('#DynamicForm').modal('hide');
	});

});

