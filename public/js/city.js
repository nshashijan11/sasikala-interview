$(document).ready(function(){


	$.ajaxSetup({
		headers:{
			'X-CSRF-TOKEN' :$('meta[name="csrf-token"]').attr('content')
		}
	});

	$('#countries').change(function(){
    	var country_id = $(this).val(); 

    	
   
	    if(country_id){
	        $.ajax({
	           type:"POST",
	           url:"getStateFromCountry",
	           data: {'country_id' : country_id},
	           success:function(res){               
	            if(res){
	                $("#states").empty();
	                $("#states").append('<option>Select State</option>');
	                $.each(res,function(key,value){
	                    $("#states").append('<option value="'+value.id+'">'+value.state_name+'</option>');
	                });
	           
	            }else{
	               $("#states").empty();
	            }
	           }
	        });
	    }else{
	        $("#states").empty();
	        $("#cities").empty();
	    }      
	  });

	 $('#myForm').on('submit', function(e) {

       e.preventDefault(); 
       var city_id = $('#id').val();    
       var city_name = $('#city_name').val();    
       if($.trim(city_name) == ''){
       		alert('Please Enter the City Name');
       		$('#city_name').val('');
       		$('#city_name').focus();
       }   
       
       $.ajax({
           type: "POST",
           url: 'submitCity',
           data: $('#myForm').serialize(),
           success: function( data ) {
	           	var obj = jQuery.parseJSON(data);
	           	if (obj.err === 'true'){
	           		alert(obj.msg);
	           		window.location.reload();
	           	}else if (obj.err=== 'false'){
	           		alert(obj.msg);
	           		$('country_name').focus();
	           	}
	       }
  		});

	});

	 $('.editCity').on('click', function(e) {
	 	var city_id = $(this).attr('data-id');
	    $.ajax({
			type: "GET",
			url: 'getCityDetails',
			data: {'city_id':city_id},
			success: function( data ) {
			   	if (data){
			       	$.each(data,function(key,value){
			       		if(key == 'country_id'){
			       			$("#countries option[value='"+value+"']").attr("selected", "selected");
			       		}else if(key == 'state_id'){
			       			$("#states option[value='"+value+"']").attr("selected", "selected");
			       		}else if(key == 'city_name'){
			       			$("#city_name").val(value);
			       		}else if(key == 'id'){
			       			$("#id").val(value);
			       		}
			        });
			   	}else{
			   		alert('Error Occurrecd!');
			   	}
			}
		});

	 });

	 $('.deleteCity').on('click', function(e) {
	 	var city_id = $(this).attr('data-id');
	    if (confirm("Are you sure to delete?")) { 
	       	$.ajax({
				type: "GET",
				url: 'deleteCityDetails',
				data: {'city_id':city_id},
				success: function( msg ) {
				   	if (msg == 'true'){
				       	alert('City Deleted!');
				       	 window.location.reload();
				   	}else{
				   		alert('Error Occurrecd!');
				   	}
				}
			});
	    } 

	 });
	

});

