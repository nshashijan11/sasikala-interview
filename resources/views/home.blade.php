@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <ul>
                        <li><a href="countries">Countries</a></li>
                        <li><a href="states">States</a></li>
                        <li><a href="city">City</a></li>
                        <li><a href="others">Other Levels of Location</a></li>
                        <li><a href="list">List of Countries, States and Cities</a></li>

                    </ul>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
