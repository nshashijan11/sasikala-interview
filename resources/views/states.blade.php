@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">CRUD operation for State <span class="pull-right"><a href="home">Back</a></span></div>

                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <th>S.No </th>
                            <th>State Name</th>
                            <th>Country</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @if($states)
                                @foreach($states as $key => $value)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$value->state_name}}</td>
                                        <td>{{$value->countries->country_name}}</td>
                                        <td>
                                            <a href="#" class="editState" data-id ="{{$value->id}}">Edit</a> | 
                                            <a href="#" class="deleteState" data-id ="{{$value->id}}">Delete</a>
                                        </td>
                                    </tr>                            
                                @endforeach
                            @endif
                            
                        </tbody>
                    </table>
                    <hr>
                    <h4>Add State</h4>
                    <form class="form-horizontal" id="myForm" name="myForm">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('countries') ? ' has-error' : '' }}">
                            <label for="countries" class="col-md-4 control-label">Countries</label>

                            <div class="col-md-6">
                                <select  id="countries" class="form-control" name="countries"  required autofocus>
                                    <option value='' class="form-control">Select Country</option>
                                        @if($countries)
                                            @foreach($countries as $k => $v)
                                                <option value="{{$v->id}}">{{$v->country_name}}</option>
                                            @endforeach
                                        @endif
                                </select>

                                @if ($errors->has('countries'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('countries') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('states') ? ' has-error' : '' }}">
                            <label for="state_name" class="col-md-4 control-label">States</label>

                            <div class="col-md-6">
                                <input  id="state_name" type="text" class="form-control" name="state_name" value='' required>
                                <input  id="id" type="hidden" class="form-control" name="id" value='' >

                                @if ($errors->has('states'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('states') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" id="submitBtn">
                                    Submit
                                </button>

                            </div>
                        </div>
                    </form>


                    
                </div>
            </div>
        </div>
    </div>
</div>
 <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
 <script src="{{ asset('js/states.js') }}"></script>
@endsection
