@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Countries, States, Cities List<span class="pull-right"><a href="home">Back</a></span></div>

                <div class="panel-body">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">



                      @if($ccs_list)
                        @foreach($ccs_list as $key => $value)
                          
                          <ul>
                            @if($value->country_name == $coun)                                  
                                <ul>  
                                    @if($value->state_name == $stat)                                      
                                      <ul>
                                        <li>{{$value->city_name}}</li>
                                        <ul>
                                          <li>{{$value->area_name}}</li>
                                          @if($value->address_id)
                                          <?php $sno = 0 ;?>
                                            <ul>
                                              @foreach(json_decode($value->address_id) as $k => $v)
                                                    <?php if($sno != 0){ ?>
                                                      <ul>
                                                        <li>{{$v}}</li>
                                                    <?php }else{ ?>
                                                      <li>{{$v}}</li>
                                                    <?php }$sno++; ?>
                                              @endforeach
                                            </ul>
                                          @endif
                                        </ul>
                                      </ul>
                                    @else
                                      <li>{{$value->state_name}}</li>
                                      <ul>
                                        <li>{{$value->city_name}}</li>
                                        <ul>
                                          <li>{{$value->area_name}}</li>
                                          @if($value->address_id)
                                          <?php $sno = 0 ;?>
                                            <ul>
                                              @foreach(json_decode($value->address_id) as $k => $v)
                                                    <?php if($sno != 0){ ?>
                                                      <ul>
                                                        <li>{{$v}}</li>
                                                    <?php }else{ ?>
                                                      <li>{{$v}}</li>
                                                    <?php }$sno++; ?>
                                              @endforeach
                                            </ul>
                                          @endif
                                        </ul>
                                      </ul>
                                    @endif
                                  </ul>
                            @else
                              <li>{{$value->country_name}}</li>
                              <ul>  
                                <li>{{$value->state_name}}</li>
                                <ul>
                                  <li>{{$value->city_name}}</li>
                                  <ul>
                                    <li>{{$value->area_name}}</li>
                                    @if($value->address_id)
                                      <?php $sno = 0 ;?>
                                        <ul>
                                          @foreach(json_decode($value->address_id) as $k => $v)
                                                <?php if($sno != 0){ ?>
                                                  <ul>
                                                    <li>{{$v}}</li>
                                                <?php }else{ ?>
                                                  <li>{{$v}}</li>
                                                <?php } $sno++;?>
                                          @endforeach
                                        </ul></ul>
                                      @endif
                                  </ul>
                                </ul>
                              </ul>

                            @endif
                              
                          </ul>
                          <?php 
                            $coun = $value->country_name;
                            $stat = $value->state_name;
                            // echo "Hie: " .$coun ."</br>";
                            // echo "Hie: " .$stat ."</br>";
                          ?>
                        @endforeach
                      @endif

                      

                     
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection




<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>

