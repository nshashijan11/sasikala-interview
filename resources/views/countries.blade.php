@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">CRUD operation for Country <span class="pull-right"><a href="home">Back</a></span></div>

                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <th>S.No </th>
                            <th>Country Name</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @if($countries)
                                @foreach($countries as $key => $value)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$value->country_name}}</td>
                                        <td>
                                            <a href="#" class="editCountry" data-id ="{{$value->id}}">Edit</a> | 
                                            <a href="#" class="deleteCountry" data-id ="{{$value->id}}">Delete</a>
                                        </td>
                                    </tr>                            
                                @endforeach
                            @endif
                            
                        </tbody>
                    </table>
                    <hr>
                    <h4>Add Country</h4>
                    <form class="form-horizontal" id="myForm" name="myForm">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('countries') ? ' has-error' : '' }}">
                            <label for="country_name" class="col-md-4 control-label">Countries</label>

                            <div class="col-md-6">
                               <input  id="country_name" type="text" class="form-control" name="country_name" value='' required>
                                <input  id="id" type="hidden" class="form-control" name="id" value='' >

                                @if ($errors->has('countries'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('countries') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" id="submitBtn">
                                    Submit
                                </button>

                            </div>
                        </div>
                    </form>


                    
                </div>
            </div>
        </div>
    </div>
</div>
 <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
 <script src="{{ asset('js/countries.js') }}"></script>
@endsection
