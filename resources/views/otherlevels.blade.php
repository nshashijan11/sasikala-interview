@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('css/common.css') }}"/>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">CRUD operation for City<span class="pull-right"><a href="home">Back</a></span></div>

                <div class="panel-body">

                    <table class="table">
                        <thead>
                            <th>S.No </th>
                            <th>Country</th>
                            <th>State </th>
                            <th>City </th>
                            <th>Area </th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @if($others)
                                @foreach($others as $key => $value)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$value->countries->country_name}}</td>
                                        <td>{{$value->states->state_name}}</td>
                                        <td>{{$value->cities->city_name}}</td>
                                        <td>{{$value->area_name}}</td>          
                                        <td>
                                            <a href="#" class="editOthers" data-id ="{{$value->id}}">Edit</a> | 
                                            <a href="#" class="deleteOthers" data-id ="{{$value->id}}">Delete</a>
                                        </td>
                                    </tr>                            
                                @endforeach
                            @endif
                            
                        </tbody>
                    </table>
                    <hr>
                    <h4>Add Other Locations</h4>
                    <form class="form-horizontal" id="myForm" name="myForm">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('countries') ? ' has-error' : '' }}">
                            <label for="countries" class="col-md-4 control-label">Country</label>

                            <div class="col-md-6">
                                <select  id="countries" class="form-control" name="countries"  required autofocus>
                                    <option value='' class="form-control">Select Country</option>
                                    @if($countries)
                                        @foreach($countries as $k => $v)

                                            <option value="{{$v->id}}">{{$v->country_name}}</option>
                                        }
                                        }
                                        @endforeach
                                    @endif
                                </select>

                                @if ($errors->has('countries'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('countries') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('states') ? ' has-error' : '' }}">
                            <label for="states" class="col-md-4 control-label">State</label>

                            <div class="col-md-6">
                                <select  id="states" class="form-control" name="states"  required>
                                    <option value='' class="form-control">Select State</option>
                                    @if($states)
                                        @foreach($states as $k => $v)
                                            <option value="{{$v->id}}">{{$v->state_name}}</option>
                                        @endforeach
                                    @endif
                                </select>

                                @if ($errors->has('states'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('states') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('city_name') ? ' has-error' : '' }}">
                            <label for="city_name" class="col-md-4 control-label">City</label>

                            <div class="col-md-6">

                                <select  id="cities" class="form-control" name="cities"  required>
                                    <option value='' class="form-control">Select City</option>
                                    @if($cities)
                                    {{$cities}}
                                        @foreach($cities as $k => $v)
                                            <option value="{{$v->id}}">{{$v->city_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('city_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('city_name') ? ' has-error' : '' }}">
                            <label for="city_name" class="col-md-4 control-label">Area</label>

                            <div class="col-md-6">                               

                                <input  id="area_name" type="text" class="form-control" name="area_name" value='' required>
                                <input  id="id" type="hidden" class="form-control" name="id" value='' >
                                                                  

                                @if ($errors->has('area_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('area_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <span>
                                <i class="fa fa-plus-square add_dynamic" data-toggle="modal" data-target="#DynamicForm" title="Add N-level of Locations"></i>
                            </span>
                        </div> 
                        <div class="dynamic_fields"></div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" id="submitBtn">
                                    Submit
                                </button>                                
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
 <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
 <script src="{{ asset('js/others.js') }}"></script>


<div id="DynamicForm" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Dynamic Fields</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="POST" id="modalDynamicForm" name="modalDynamicForm" action="">
                    <div class="form-group">
                        <label class="control-label">Dynamic Label Name</label>
                        <div>
                            <input type="text" class="form-control input" name="label_name" id="label_name" placeholder="Street" value="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Dynamic Label Value</label>
                        <div>
                            <input type="text" class="form-control input" name="label_value" id="label_value" placeholder="Twinkle Street" value="" required>
                        </div>
                    </div>                    
                    <div class="form-group">
                        <div>
                            <button type="submit" class="btn btn-success">Add Field</button>
                         </div>
                    </div>
                </form>                
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection
