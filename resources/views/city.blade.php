@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">CRUD operation for City<span class="pull-right"><a href="home">Back</a></span></div>

                <div class="panel-body">

                    <table class="table">
                        <thead>
                            <th>S.No </th>
                            <th>City Name</th>
                            <th>State Name</th>
                            <th>Country Name</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @if($cities)
                                @foreach($cities as $key => $value)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$value->city_name}}</td>
                                        <td>{{$value->states->state_name}}</td>
                                        <td>{{$value->countries->country_name}}</td>
                                        <td>
                                            <a href="#" class="editCity" data-id ="{{$value->id}}">Edit</a> | 
                                            <a href="#" class="deleteCity" data-id ="{{$value->id}}">Delete</a>
                                        </td>
                                    </tr>                            
                                @endforeach
                            @endif
                            
                        </tbody>
                    </table>
                    <hr>
                    <h4>Add City</h4>
                    <form class="form-horizontal" id="myForm" name="myForm">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('countries') ? ' has-error' : '' }}">
                            <label for="countries" class="col-md-4 control-label">Countries</label>

                            <div class="col-md-6">
                                <select  id="countries" class="form-control" name="countries"  required autofocus>
                                    <option value='' class="form-control">Select Country</option>
                                    @if($countries)
                                        @foreach($countries as $k => $v)

                                            <option value="{{$v->id}}">{{$v->country_name}}</option>
                                        }
                                        }
                                        @endforeach
                                    @endif
                                </select>

                                @if ($errors->has('countries'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('countries') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('states') ? ' has-error' : '' }}">
                            <label for="states" class="col-md-4 control-label">States</label>

                            <div class="col-md-6">
                                <select  id="states" class="form-control" name="states"  required>
                                    <option value='' class="form-control">Select State</option>
                                    @if($states)
                                        @foreach($states as $k => $v)
                                            <option value="{{$v->id}}">{{$v->state_name}}</option>
                                        @endforeach
                                    @endif
                                </select>

                                @if ($errors->has('states'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('states') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('city_name') ? ' has-error' : '' }}">
                            <label for="city_name" class="col-md-4 control-label">City</label>

                            <div class="col-md-6">
                                <input  id="city_name" type="text" class="form-control" name="city_name" value='' required>
                                <input  id="id" type="hidden" class="form-control" name="id" value='' >
                                                                  

                                @if ($errors->has('city_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" id="submitBtn">
                                    Submit
                                </button>

                            </div>
                        </div>
                    </form>


                    
                </div>
            </div>
        </div>
    </div>
</div>
 <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
 <script src="{{ asset('js/city.js') }}"></script>
@endsection
